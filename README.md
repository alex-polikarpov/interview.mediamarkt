# Тестовое задание для Media Markt. 

Выполнял в августе 2015 года. 



#### Документация

* [Описание тестового задания тут](https://gitlab.com/23Alex24/interview.mediamarkt/blob/master/docs/home.md) и в файле **Задание_2_C#  от MediaMarkt.doc** в корне проекта.
* [Скриншоты](https://gitlab.com/23Alex24/interview.mediamarkt/blob/master/docs/screenshots.md)
* [Что можно улучшить, в чем ошибки](https://gitlab.com/23Alex24/interview.mediamarkt/blob/master/docs/analysis.md)

#### Запуск:

* файл БД сохраняется в папку AppData (в самом проекте), устанавливать для БД ничего не надо;
* При каждом запуске происходит удаление и создание БД, чтобы можно было возвращаться к исходному состоянию;
* Запускать в VS 2015 или VS 2017;

#### Стек технологий:

* .NET Framework 4.5, ASP.NET MVC 5, Autofac (IoC container);
* SQL SERVER Compact Edition, Entity Framework 6;
* Луковая (Onion) архитектура (первый проект на такой архитектуре пытался сделать);
* Html, Css, Jquery, Bootstrap.

