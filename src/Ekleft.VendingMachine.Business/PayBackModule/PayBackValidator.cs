﻿using System;
using System.Linq;
using VendingMachineEntity = Ekleft.VendingMachine.Core.Entities.VendingMachine;

namespace Ekleft.VendingMachine.Business
{
    public class PayBackValidator
    {
        public bool IsEnough(VendingMachineEntity machine)
        {
            int deposited = machine.DepositedAmount; //сколько должны дать сдачи
            int residual = deposited; //Сколько средств еще нужно отдать

            if (deposited < 0)
                return true;

            //сначала отдаем самые крупные монеты
            var ordered = machine.VendingMachineCoinLinks
                .OrderByDescending(x => x.Coin.CoinValue)
                .ToList();

            foreach (var coin in ordered)
            {
                if (coin.Count < 1)
                    continue;

                //сколько целых монет можем отдать
                var coinsCount = Convert.ToInt32(Math.Floor((decimal)(residual / (int)coin.Coin.CoinValue)));

                if (coinsCount == 0)
                    continue;

                if (coin.Count < coinsCount)
                    coinsCount = coin.Count;

                residual -= coinsCount * (int)coin.Coin.CoinValue;

                if (residual == 0)
                    break;
            }

            return residual == 0;
        }
    }
}
