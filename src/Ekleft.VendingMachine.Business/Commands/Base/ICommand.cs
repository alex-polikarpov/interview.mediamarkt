﻿using System.Threading.Tasks;

namespace Ekleft.VendingMachine.Business
{
    /// <summary>Команда, выполняющая какие-либо действия</summary>
    /// <typeparam name="TArguments">Тип входных аргументов для команды</typeparam>
    public interface ICommand<TArguments> 
        where TArguments : ICommandArguments
    {
        Task<ICommandResult> Execute(TArguments arguments);

        Task<ICommandResult> Validate(TArguments arguments);
    }
}
