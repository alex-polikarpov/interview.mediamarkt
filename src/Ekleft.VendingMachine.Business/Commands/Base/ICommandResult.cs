﻿namespace Ekleft.VendingMachine.Business
{
    /// <summary>Результат выполнения команды</summary>
    public interface ICommandResult
    {
        /// <summary>True, если команда завешилась успешно, иначе false</summary>
        bool IsSuccess { get; set; }

        /// <summary>Информация об ошибке</summary>
        CommadError Error { get; set; }
    }
}
