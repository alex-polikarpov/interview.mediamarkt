﻿namespace Ekleft.VendingMachine.Business
{
    public abstract class BaseCommandResult : ICommandResult
    {
        public bool IsSuccess { get; set; }

        public CommadError Error { get; set; }

        public ICommandResult AddError(string error)
        {
            IsSuccess = false;
            Error = new CommadError { ErrorMessage = error };
            return this;
        }
    }
}
