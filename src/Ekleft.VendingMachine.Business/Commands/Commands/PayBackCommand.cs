﻿using Ekleft.VendingMachine.Core.Entities;
using Ekleft.VendingMachine.Core.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VendingMachineEntity = Ekleft.VendingMachine.Core.Entities.VendingMachine;

namespace Ekleft.VendingMachine.Business
{
    public class PayBackCommand : BaseCommand<PayBackArguments>
    {
        private readonly IVendingMachineService _machineService;
        private readonly IUserService _userService;
        private User _user;
        private VendingMachineEntity _machine;

        public PayBackCommand(IUnitOfWork uow,
            IVendingMachineService machineService, IUserService userService) : base(uow)
        {
            if (machineService == null || userService == null)
                throw new ArgumentNullException();

            _machineService = machineService;
            _userService = userService;
        }


        public override async Task<ICommandResult> Execute(PayBackArguments arguments)
        {
            var result = await Validate(arguments);

            if (!result.IsSuccess)
                return result;

            var calculator = new PayBackCalculator();
            var caclRes = calculator.Calculate(_machine);

            await _machineService.PayBack(_user.Id, _machine.Id, caclRes);

            return result;
        }

        public override async Task<ICommandResult> Validate(PayBackArguments arguments)
        {
            var result = new EmptyCommandResult { IsSuccess = true };

            var userId = _userService.GetUserId();
            var machineId = _machineService.GetMachineId();

            await Task.WhenAll(userId, machineId);

            var userInfoTask = _userService.GetUserInfo(userId.Result);
            var machineInfo = _machineService.GetMachineInfo(machineId.Result);

            await Task.WhenAll(userInfoTask, machineInfo);

            _user = userInfoTask.Result;
            _machine = machineInfo.Result;

            if (_machine.DepositedAmount < 1)
                return result.AddError("В автомате нет остатка");

            var validator = new PayBackValidator();

            if(!validator.IsEnough(_machine))
                return result.AddError("В торговом автомате недостаточно средств для сдачи");

            return result;
        }
    }
}
