﻿using Ekleft.VendingMachine.Core.Entities;
using Ekleft.VendingMachine.Core.Services;
using System;
using System.Linq;
using System.Threading.Tasks;
using VendingMachineEntity = Ekleft.VendingMachine.Core.Entities.VendingMachine;

namespace Ekleft.VendingMachine.Business
{
    public class BuyProductCommand : BaseCommand<BuyProductArguments>
    {
        private readonly IVendingMachineService _machineService;
        private VendingMachineEntity _machine;
        private VendingMachineProductLink _product;

        public BuyProductCommand(IUnitOfWork uow, IVendingMachineService machineService) : base(uow)
        {
            if (machineService == null)
                throw new ArgumentNullException();

            _machineService = machineService;
        }

        public override async Task<ICommandResult> Execute(BuyProductArguments arguments)
        {
            var result = await Validate(arguments);

            if (!result.IsSuccess)
                return result;

            _product.Count--;
            _machine.DepositedAmount -= _product.Product.Cost;
            await UnitOfWork.SaveChangesAsync();

            return result;
        }

        public override async Task<ICommandResult> Validate(BuyProductArguments arguments)
        {
            if (arguments == null)
                throw new ArgumentNullException();

            var result = new EmptyCommandResult() { IsSuccess = true };

            var id = await _machineService.GetMachineId();
            _machine = await _machineService.GetMachineInfo(id);

            _product = await _machineService.GetProduct(id, arguments.ProductId);

            if (_product == null || _product.Count < 1)
                return result.AddError("В автомате нет этого продукта");

            if (_machine.DepositedAmount < _product.Product.Cost)
                return result.AddError("Недостаточно средств");

            return result;
        }
    }
}
