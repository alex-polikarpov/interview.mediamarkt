﻿using Ekleft.VendingMachine.Core.Entities;
using Ekleft.VendingMachine.Core.Services;
using System;
using System.Threading.Tasks;
using VendingMachineEntity = Ekleft.VendingMachine.Core.Entities.VendingMachine;

namespace Ekleft.VendingMachine.Business
{
    public class PutCoinCommand :  BaseCommand<PutCoinArguments>
    {
        private readonly IVendingMachineService _machineService;
        private readonly IUserService _userService;
        private VendingMachineEntity _machine;
        private UserCoinLink _userCoin;

        public PutCoinCommand(IUnitOfWork uow,
            IVendingMachineService machineService, IUserService userService) : base(uow)
        {
            if (machineService == null || userService == null)
                throw new ArgumentNullException();

            _machineService = machineService;
            _userService = userService;
        }

        public override async Task<ICommandResult> Execute(PutCoinArguments arguments)
        {
            var result = await Validate(arguments);

            if (!result.IsSuccess)
                return result;

            var userCoinRepository = UnitOfWork.GetRepository<UserCoinLink>();
            var machineCoinRepository = UnitOfWork.GetRepository<VendingMachineCoinLink>();

            _userCoin.Count--;

            var machineCoin = await _machineService.GetVendingMachineCoin(_machine.Id, arguments.CoinValue);
            if(machineCoin == null)
            {
                machineCoin = new VendingMachineCoinLink
                {
                    CoinId = _userCoin.CoinId,
                    Count = 1,
                    VendingMachineId = _machine.Id
                };

                machineCoinRepository.Add(machineCoin);
            }
            else machineCoin.Count++;            

            _machine.DepositedAmount += (int)_userCoin.Coin.CoinValue;
            await UnitOfWork.SaveChangesAsync();

            return result;
        }



        public override async Task<ICommandResult> Validate(PutCoinArguments arguments)
        {
            if (arguments == null)
                throw new ArgumentNullException();

            var result = new EmptyCommandResult() { IsSuccess = true };
            var userIdTask = _userService.GetUserId();
            var machineIdTask = _machineService.GetMachineId();

            await Task.WhenAll(userIdTask, machineIdTask);

            var userCoinTask = _userService.GetUserCoin(userIdTask.Result, arguments.CoinValue);
            var machineTask = _machineService.GetMachineInfo(machineIdTask.Result);

            await Task.WhenAll(userCoinTask, machineTask);

            _userCoin = userCoinTask.Result;
            _machine = machineTask.Result;

            if (_userCoin == null || _userCoin.Count < 1)
                return result.AddError("У пользователя недостаточно средств");                

            return result;
        }
    }
}
