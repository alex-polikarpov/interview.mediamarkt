﻿using Ekleft.VendingMachine.Core.Entities;

namespace Ekleft.VendingMachine.Business
{
    /// <summary>Аргументы для команды "положить монету в автомат"</summary>
    public class PutCoinArguments : ICommandArguments
    {
        public CoinValue CoinValue { get; set; }
    }
}
