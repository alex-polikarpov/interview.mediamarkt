﻿namespace Ekleft.VendingMachine.Business
{
    public class BuyProductArguments : ICommandArguments
    {
        public int ProductId { get; set; }
    }
}
