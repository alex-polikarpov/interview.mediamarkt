﻿using Autofac;
using Ekleft.VendingMachine.Business;

namespace Ekleft.VendingMachine.Infrastructure.DependencyResolution
{
    public class BusinessModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<PutCoinCommand>().As<ICommand<PutCoinArguments>>();
            builder.RegisterType<BuyProductCommand>().As<ICommand<BuyProductArguments>>();
            builder.RegisterType<PayBackCommand>().As<ICommand<PayBackArguments>>();
        }
    }
}
