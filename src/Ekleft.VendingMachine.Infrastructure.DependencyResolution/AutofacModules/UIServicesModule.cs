﻿using Autofac;
using Ekleft.VendingMachine.UI.Model;
using Ekleft.VendingMachine.UI.Services;

namespace Ekleft.VendingMachine.Infrastructure.DependencyResolution
{
    public class UIServicesModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {            
            builder.RegisterType<HomeIndexModelBuilder>().As<IModelBuilder<IndexModel, EmptyArguments>>();
        }
    }
}