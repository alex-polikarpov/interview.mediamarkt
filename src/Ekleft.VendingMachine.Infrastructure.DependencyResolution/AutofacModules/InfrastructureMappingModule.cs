﻿using Autofac;
using Ekleft.VendingMachine.Infrastructure.Mapping;
using Ekleft.VendingMachine.Services;

namespace Ekleft.VendingMachine.Infrastructure.DependencyResolution
{
    public class InfrastructureMappingModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<AutofacMapper>().As<IMapper>().InstancePerRequest();
        }
    }
}
