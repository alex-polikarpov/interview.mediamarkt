﻿using Ekleft.VendingMachine.Core.Entities;
using System.ComponentModel.DataAnnotations;

namespace EkleftVendingMachine.Requests
{
    public class AddCoinRequest
    {
        [Required(ErrorMessage="Необходимо указать монету")]
        [EnumDataType(typeof(CoinValue),ErrorMessage="Монета не существует")]
        public CoinValue CoinValue { get; set; }
    }
}