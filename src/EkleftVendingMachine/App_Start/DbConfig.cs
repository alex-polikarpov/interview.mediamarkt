﻿using Ekleft.VendingMachine.Infrastructure.Data.EF;
using System.Data.Entity;

namespace EkleftVendingMachine
{
    public class DbConfig
    {
        public static void Register()
        {
            Database.SetInitializer(new VendingMachineInitializer());
        }
    }
}