﻿using Ekleft.VendingMachine.Infrastructure.Mapping;

namespace EkleftVendingMachine.App_Start
{
    public class MappingConfig
    {
        public static void Configure()
        {
            UIServiceMap.Configure();
        }
    }
}