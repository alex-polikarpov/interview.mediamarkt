﻿using Ekleft.VendingMachine.Business;
using Ekleft.VendingMachine.Business.Commands;
using Ekleft.VendingMachine.UI.Services;
using EkleftVendingMachine.Requests;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace EkleftVendingMachine.Controllers
{
    /*
     * Я бы здесь использовал jquery, но к сожалению не 
     * было времени возиться с клиентской частью поэтому с перезагрузкой
     */
    public class MachineController : Controller
    {
        /// <summary>Добавляет монетку в торговый автомат</summary>
        [HttpPost]
        public async Task<ActionResult> PutCoin(AddCoinRequest request)
        {
            var command = DependencyResolver.Current.GetService<ICommand<PutCoinArguments>>();

            if (!ModelState.IsValid)
            { 
                TempData[KeysConstants.ErrorMessageKey] = "Неверный запрос";
                return RedirectToAction("Index", "Home");
            }

            var result = await command.Execute(new PutCoinArguments { CoinValue = request.CoinValue });

            if(result.IsSuccess)
            {
                TempData[KeysConstants.SuccessMessageKey] = "Монета успешно добавлена в автомат";
            }
            else TempData[KeysConstants.ErrorMessageKey] = result.Error.ErrorMessage;
            return RedirectToAction("Index", "Home");
        }

        [HttpPost]
        public async Task<ActionResult> BuyProduct(int productId)
        {
            var command = DependencyResolver.Current.GetService<ICommand<BuyProductArguments>>();

            if (productId < 1)
            {
                TempData[KeysConstants.ErrorMessageKey] = "Неверный запрос";
                return RedirectToAction("Index", "Home");
            }

            var result = await command.Execute(new BuyProductArguments { ProductId = productId });

            if (result.IsSuccess)
            {
                TempData[KeysConstants.SuccessMessageKey] = "Спасибо!";
            }
            else TempData[KeysConstants.ErrorMessageKey] = result.Error.ErrorMessage;

            return RedirectToAction("Index", "Home");
        }

        [HttpPost]
        public async Task<ActionResult> PayBack()
        {
            var command = DependencyResolver.Current.GetService<ICommand<PayBackArguments>>();
            var result = await command.Execute(new PayBackArguments { });

            if(!result.IsSuccess)
            {
                TempData[KeysConstants.ErrorMessageKey] = result.Error.ErrorMessage;
                return RedirectToAction("Index", "Home");
            }

            TempData[KeysConstants.SuccessMessageKey] = "Сдача успешно возвращена";
            return RedirectToAction("Index", "Home");
        }
    }
}