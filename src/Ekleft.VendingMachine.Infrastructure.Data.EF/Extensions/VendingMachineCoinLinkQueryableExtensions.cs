﻿using Ekleft.VendingMachine.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ekleft.VendingMachine.Infrastructure.Data.EF.Extensions
{
    public static class VendingMachineCoinLinkQueryableExtensions
    {
        /// <summary>Фильтрует монеты автомата по VendingMachineId</summary>
        /// <param name="id">id автомата</param>
        public static IQueryable<VendingMachineCoinLink> FilterByVendingMachineId(
            this IQueryable<VendingMachineCoinLink> query, int id)
        {
            return query.Where(x => x.VendingMachineId == id);
        }

        public static IQueryable<VendingMachineCoinLink> FilterByCoinValue(
            this IQueryable<VendingMachineCoinLink> query, CoinValue value)
        {
            return query.Where(x => x.Coin.CoinValue == value);
        }
    }
}
