﻿using Ekleft.VendingMachine.Core.Entities;
using System;
using System.Linq;

namespace Ekleft.VendingMachine.Infrastructure.Data.EF.Extensions
{
    internal static class UserQueryableExtensions
    {
        /// <summary>Фильтрует пользователя по его Id</summary>
        internal static IQueryable<User> FilterById(this IQueryable<User> query, Guid id)
        {
            return query.Where(x => x.Id == id);
        }
    }
}
