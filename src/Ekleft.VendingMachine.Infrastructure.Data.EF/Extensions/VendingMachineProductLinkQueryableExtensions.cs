﻿using Ekleft.VendingMachine.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ekleft.VendingMachine.Infrastructure.Data.EF.Extensions
{
    public static class VendingMachineProductLinkQueryableExtensions
    {
        /// <summary>Фильтрует продукты автомата по VendingMachineId</summary>
        /// <param name="id">VendingMachineId</param>
        public static IQueryable<VendingMachineProductLink> FilterByVendingMachineId(
            this IQueryable<VendingMachineProductLink> query, int id)
        {
            return query.Where(x => x.VendingMachineId == id);
        }

        public static IQueryable<VendingMachineProductLink> FilterByProductId(
            this IQueryable<VendingMachineProductLink> query, int id)
        {
            return query.Where(x => x.ProductId == id);
        }
    }
}
