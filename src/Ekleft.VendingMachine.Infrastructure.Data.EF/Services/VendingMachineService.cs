﻿using Ekleft.VendingMachine.Core.Entities;
using Ekleft.VendingMachine.Core.Services;
using Ekleft.VendingMachine.Infrastructure.Data.EF.Extensions;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using VendingMachineEntity = Ekleft.VendingMachine.Core.Entities.VendingMachine;

namespace Ekleft.VendingMachine.Infrastructure.Data.EF.Services
{
    public class VendingMachineService : ServiceBase, IVendingMachineService
    {
        public VendingMachineService(IUnitOfWork unitOfWork) : base(unitOfWork) { }

        public async Task<int> GetMachineId()
        {
            var repository = UnitOfWork.GetRepository<VendingMachineEntity>();
            return await repository.Select().Select(x => x.Id).FirstAsync();
        }

        public async Task<VendingMachineEntity> GetMachineInfo(int vendingMachineId)
        {
            var machineRepository = UnitOfWork.GetRepository<VendingMachineEntity>();
            var productRepository = UnitOfWork.GetRepository<VendingMachineProductLink>();
            var coinsRepository = UnitOfWork.GetRepository<VendingMachineCoinLink>();

            var machine = await machineRepository.Select()
                .FilterById(vendingMachineId)
                .FirstAsync();

            var coinsTask = coinsRepository.Select()
                .FilterByVendingMachineId(vendingMachineId)
                .Include(x => x.Coin)
                .ToListAsync();

            var productsTask = productRepository.Select()
                .FilterByVendingMachineId(vendingMachineId)
                .Include(x => x.Product)
                .ToListAsync();

            await Task.WhenAll(coinsTask, productsTask);

            machine.VendingMachineCoinLinks = coinsTask.Result;
            machine.VendingMachineProductLinks = productsTask.Result;
            return machine;
        }

        public async Task<VendingMachineCoinLink> GetVendingMachineCoin(
            int vendingMachineId, CoinValue coinValue)
        {
            var repository = UnitOfWork.GetRepository<VendingMachineCoinLink>();

            return await repository.Select()
                .FilterByVendingMachineId(vendingMachineId)
                .FilterByCoinValue(coinValue)
                .FirstOrDefaultAsync();
        }

        public async Task<VendingMachineProductLink> GetProduct(int vendingMachineId, int productId)
        {
            var repository = UnitOfWork.GetRepository<VendingMachineProductLink>();
            return await repository.Select()
                 .Include(x => x.Product)
                 .FilterByVendingMachineId(vendingMachineId)
                 .FilterByProductId(productId)
                 .FirstOrDefaultAsync();
        }

        public async Task PayBack(Guid userId, int machineId, Dictionary<CoinValue, int> coins)
        {
            var coinsRepository = UnitOfWork.GetRepository<Coin>();
            var userCoinsRepository = UnitOfWork.GetRepository<UserCoinLink>();

            var coinsTask = coinsRepository.Select().ToListAsync();

            var userCoinsTask = userCoinsRepository.Select()
                .FilterByUserId(userId)
                .Include(x => x.Coin)
                .ToListAsync();

            var machineTask = GetMachineInfo(machineId);

            await Task.WhenAll(coinsTask, userCoinsTask, machineTask);

            foreach (var coin in coins)
            {
                var dbCoin = coinsTask.Result.Where(x => x.CoinValue == coin.Key).FirstOrDefault();

                if (dbCoin == null)
                    continue;

                var machineCoin = machineTask.Result.VendingMachineCoinLinks
                    .Where(x => x.CoinId == dbCoin.Id)
                    .FirstOrDefault();

                if (machineCoin == null)
                    throw new Exception("В автомате недостаточно монет");

                machineCoin.Count -= coin.Value;

                var userCoin = userCoinsTask.Result
                    .Where(x => x.Coin.CoinValue == coin.Key)
                    .FirstOrDefault();

                if (userCoin == null)
                {
                    userCoin = new UserCoinLink
                    {
                        CoinId = dbCoin.Id,
                        UserId = userId,
                        Count = coin.Value
                    };

                    userCoinsRepository.Add(userCoin);
                }
                else userCoin.Count += coin.Value;
            }

            machineTask.Result.DepositedAmount = 0;
            await UnitOfWork.SaveChangesAsync();
        }
    }
}