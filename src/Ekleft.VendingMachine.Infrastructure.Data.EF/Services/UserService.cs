﻿using Ekleft.VendingMachine.Core.Entities;
using Ekleft.VendingMachine.Core.Services;
using Ekleft.VendingMachine.Infrastructure.Data.EF.Extensions;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Threading.Tasks;
using System.Linq;

namespace Ekleft.VendingMachine.Infrastructure.Data.EF.Services
{
    public class UserService : ServiceBase, IUserService
    {
        public UserService(IUnitOfWork unitOfWork) : base(unitOfWork) { }

        public async Task<Guid> GetUserId()
        {
            var repository = UnitOfWork.GetRepository<User>();
            return await repository.Select().Select(x => x.Id).FirstAsync();
        }

        public async Task<User> GetUserInfo(Guid id)
        {
            var userRepository = UnitOfWork.GetRepository<User>();
            var repository = UnitOfWork.GetRepository<UserCoinLink>();

            var user = await userRepository.Select()
                .FilterById(id)
                .FirstAsync();

            var coins = await repository.Select()
                .FilterByUserId(id)
                .Include(x => x.Coin)
                .ToListAsync();

            user.UserCoinLinks = coins;
            return user;
        }
     
        public async Task<UserCoinLink> GetUserCoin(Guid userId, CoinValue coinValue)
        {
            var repository = UnitOfWork.GetRepository<UserCoinLink>();
            return await repository.Select()
                .FilterByUserId(userId)
                .FilterByCoinValue(coinValue)
                .FirstOrDefaultAsync();
        }
    }
}