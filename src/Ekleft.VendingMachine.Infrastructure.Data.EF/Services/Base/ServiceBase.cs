﻿using Ekleft.VendingMachine.Core.Services;
using System;

namespace Ekleft.VendingMachine.Infrastructure.Data.EF.Services
{
    public abstract class ServiceBase
    {
        private readonly IUnitOfWork _unitOfWork;

        public ServiceBase(IUnitOfWork unitOfWork)
        {
            if (unitOfWork == null)
                throw new ArgumentNullException();

            _unitOfWork = unitOfWork;
        }

        protected virtual IUnitOfWork UnitOfWork
        {
            get { return _unitOfWork; }
        }
    }
}
