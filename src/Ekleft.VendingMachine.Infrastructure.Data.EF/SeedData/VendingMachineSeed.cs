﻿using Ekleft.VendingMachine.Core.Entities;
using System.Collections.Generic;
using VendingMachineEntity = Ekleft.VendingMachine.Core.Entities.VendingMachine;

namespace Ekleft.VendingMachine.Infrastructure.Data.EF
{
    internal class VendingMachineSeed
    {
        private const int _defaultCount = 100;

        internal static VendingMachineEntity Generate(IEnumerable<Coin> coins, IList<Product> products)
        {
            var result = new VendingMachineEntity
            {
                Name = "Кофейный автомат",
                VendingMachineCoinLinks = new List<VendingMachineCoinLink>()
            };

            foreach (var coin in coins)
            {
                result.VendingMachineCoinLinks.Add(new VendingMachineCoinLink()
                    {
                        Coin = coin,
                        Count = _defaultCount
                    });
            }

            result.VendingMachineProductLinks = new List<VendingMachineProductLink>()
            {
                new VendingMachineProductLink{Product = products[0], Count=10},
                new VendingMachineProductLink{Product = products[1], Count=20},
                new VendingMachineProductLink{Product = products[2], Count=20},
                new VendingMachineProductLink{Product = products[3], Count=15}
            };

            return result;
        }
    }
}
