﻿using Ekleft.VendingMachine.Core.Entities;
using System.Collections.Generic;
using System.Data.Entity;
using VendingMachineEntity = Ekleft.VendingMachine.Core.Entities.VendingMachine;

namespace Ekleft.VendingMachine.Infrastructure.Data.EF
{
    public class VendingMachineInitializer : DropCreateDatabaseAlways<VendingMachineContext>
    {
        protected override void Seed(VendingMachineContext context)
        {
            var coins = CoinsSeed.Generate();
            var products = ProductsSeed.Generate();
            var user = UserSeed.Generate(coins);
            var machine = VendingMachineSeed.Generate(coins, products);

            context.Products.AddRange(products);
            context.Coins.AddRange(coins);
            context.Users.Add(user);
            context.VendingMachines.Add(machine);
            context.SaveChanges();
        }
    }
}
