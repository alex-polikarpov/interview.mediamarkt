﻿using Ekleft.VendingMachine.Core.Entities;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using VendingMachineEntity = Ekleft.VendingMachine.Core.Entities.VendingMachine;

namespace Ekleft.VendingMachine.Infrastructure.Data.EF
{
    public class VendingMachineContext : DbContext
    {
        /// <summary>Монеты</summary>
        public DbSet<Coin> Coins { get; set; }

        /// <summary>Пользователи</summary>
        public DbSet<User> Users { get; set; }

        /// <summary>Торговые автоматы</summary>
        public DbSet<VendingMachineEntity> VendingMachines { get; set; }

        public DbSet<Product> Products { get; set; }



        #region Links

        /// <summary>Монеты пользователей</summary>
        public DbSet<UserCoinLink> UserCoinsLinks { get; set; }

        /// <summary>Монеты торгового автомата</summary>
        public DbSet<VendingMachineCoinLink> VendingMachineCoinLinks { get; set; }

        /// <summary>Продукты торогового автомата</summary>
        public DbSet<VendingMachineProductLink> VendingMachineProductLinks { get; set; }

        #endregion




        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<UserCoinLink>()
                .HasRequired(x => x.Coin)
                .WithMany(x => x.UserCoinLinks)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<UserCoinLink>()
                .HasRequired(x => x.User)
                .WithMany(x => x.UserCoinLinks)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<VendingMachineCoinLink>()
                .HasRequired(x => x.Coin)
                .WithMany(x => x.VendingMachineCoinLinks)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<VendingMachineCoinLink>()
                .HasRequired(x => x.VendingMachine)
                .WithMany(x => x.VendingMachineCoinLinks)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<VendingMachineProductLink>()
                .HasRequired(x => x.VendingMachine)
                .WithMany(x => x.VendingMachineProductLinks)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<VendingMachineProductLink>()
                .HasRequired(x => x.Product)
                .WithMany(x => x.VendingMachineProductLinks)
                .WillCascadeOnDelete(false);
        }
    }
}
