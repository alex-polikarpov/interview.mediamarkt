﻿using Ekleft.VendingMachine.Core.Entities;

namespace Ekleft.VendingMachine.UI.Model
{
    /// <summary>Модель для отображения набора монет</summary>
    public class CoinSetModel : IViewModel
    {
        /// <summary>Достоинство монет</summary>
        public int CoinValue { get; set; }

        /// <summary>Количество монет</summary>
        public int Count { get; set; }
    }
}
