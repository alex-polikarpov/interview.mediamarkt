﻿using Ekleft.VendingMachine.Core.Entities;
using System;
using System.Collections.Generic;

namespace Ekleft.VendingMachine.UI.Model
{
    /// <summary>Модель для отображения пользователя</summary>
    public class UserModel : IViewModel
    {
        /// <summary>Id пользователя</summary>
        public Guid Id { get; set; }

        /// <summary>Монеты пользователя</summary>
        public IEnumerable<CoinSetModel> Coins { get; set; }
    }
}
