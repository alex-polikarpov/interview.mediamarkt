﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ekleft.VendingMachine.UI.Model
{
    /// <summary>Модель для отображения набора продуктов</summary>
    public class ProductSetModel : IViewModel
    {
        /// <summary>Id продукта</summary>
        public int Id { get; set; }

        /// <summary>Название продукта</summary>
        public string Name { get; set; }

        /// <summary>Стоимость продукта</summary>
        public int Cost { get; set; }

        /// <summary>Количество единиц продукта</summary>
        public int Count { get; set; }
    }
}
