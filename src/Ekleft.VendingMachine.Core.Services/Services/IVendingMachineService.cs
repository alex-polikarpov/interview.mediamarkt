﻿using Ekleft.VendingMachine.Core.Entities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using VendingMachineEntity = Ekleft.VendingMachine.Core.Entities.VendingMachine;

namespace Ekleft.VendingMachine.Core.Services
{
    public interface IVendingMachineService
    {
        /// <summary>Возвращает айди торгового автомата</summary>
        Task<int> GetMachineId();

        /// <summary>Возвращает торговый автомат и информацию о его монетах, товарах</summary>
        Task<VendingMachineEntity> GetMachineInfo(int vendingMachineId);

        /// <summary>Возвращает монеты автомата определенного достоинства</summary>
        Task<VendingMachineCoinLink> GetVendingMachineCoin(int vendingMachineId, CoinValue coinValue);

        /// <summary>Возвращает продукт автомата</summary>
        Task<VendingMachineProductLink> GetProduct(int vendingMachineId, int productId);

        /// <summary>Возвращает сдачу пользователю</summary>
        /// <param name="coins">Монеты, которые надо вернуть пользователю</param>
        Task PayBack(Guid userId, int machineId, Dictionary<CoinValue, int> coins);
    }
}
