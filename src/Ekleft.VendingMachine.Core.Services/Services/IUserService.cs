﻿using Ekleft.VendingMachine.Core.Entities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Ekleft.VendingMachine.Core.Services
{
    public interface IUserService
    {
        /// <summary>Возвращает id пользователя</summary>
        Task<Guid> GetUserId();

        /// <summary>Возвращает пользователя по айди, либо null если он не найден</summary>
        Task<User> GetUserInfo(Guid id);

        /// <summary>Возвращает монеты пользователя определенного достоинства</summary>
        Task<UserCoinLink> GetUserCoin(Guid userId, CoinValue coinValue);
    }
}
