﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Ekleft.VendingMachine.Core.Entities
{
    [Table("Users")]
    public class User : IEntity
    {
        [Key]
        public Guid Id { get; set; }

        public virtual ICollection<UserCoinLink> UserCoinLinks { get; set; }
    }
}
