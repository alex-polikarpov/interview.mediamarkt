﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ekleft.VendingMachine.Core.Entities
{
    /// <summary>Монеты, хранящиеся в торговом автомате</summary>
    [Table("VendingMachineCoinLinks")]
    public class VendingMachineCoinLink : IEntity
    {
        [Key]
        public int Id { get; set; }

        [ForeignKey("VendingMachine")]
        public int VendingMachineId { get; set; }

        [ForeignKey("Coin")]
        public int CoinId { get; set; }

        /// <summary>Количество монет в автомате</summary>
        public int Count { get; set; }

        public VendingMachine VendingMachine { get; set; }

        public Coin Coin { get; set; }
    }
}
