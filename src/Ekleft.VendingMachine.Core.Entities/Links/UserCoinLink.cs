﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Ekleft.VendingMachine.Core.Entities
{
    [Table("UserCoinLinks")]
    public class UserCoinLink : IEntity
    {
        [Key]
        public int Id { get; set; }

        [ForeignKey("User")]
        public Guid UserId { get; set; }

        [ForeignKey("Coin")]
        public int CoinId { get; set; }

        /// <summary>Количество монет у пользователя</summary>
        public int Count { get; set; }



        public virtual Coin Coin { get; set; }

        public virtual User User { get; set; }
    }
}
