﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ekleft.VendingMachine.Core.Entities
{
    /// <summary>Продукты торгового автомата</summary>
    [Table("VendingMachineProductLinks")]
    public class VendingMachineProductLink : IEntity
    {
        [Key]
        public int Id { get; set; }

        [ForeignKey("VendingMachine")]
        public int VendingMachineId { get; set; }

        [ForeignKey("Product")]
        public int ProductId { get; set; }

        /// <summary>Количество единиц товара в автомате</summary>
        public int Count { get; set; }

        public VendingMachine VendingMachine { get; set; }

        public Product Product { get; set; }
    }
}
