﻿using Ekleft.VendingMachine.Core.Entities;
using Ekleft.VendingMachine.Core.Services;
using Ekleft.VendingMachine.Services;
using Ekleft.VendingMachine.UI.Model;
using System;
using System.Threading.Tasks;

namespace Ekleft.VendingMachine.UI.Services
{
    public class VendingMachineModelBuilder : IModelBuilder<VendingMachineModel, EmptyArguments>
    {
        private readonly IVendingMachineService _service;
        private readonly IMapper _mapper;

        public VendingMachineModelBuilder(IVendingMachineService service, IMapper mapper)
        {
            if (service == null || mapper == null)
                throw new ArgumentNullException();

            _service = service;
            _mapper = mapper;
        }

        public async Task<VendingMachineModel> Build(EmptyArguments arguments = null)
        {
            var model = new VendingMachineModel();

            var id = await _service.GetMachineId();
            var machine = await _service.GetMachineInfo(id);
            model.DepositedAmount = machine.DepositedAmount;
            model.Coins = _mapper.MapEnumerable<VendingMachineCoinLink, CoinSetModel>(machine.VendingMachineCoinLinks);
            model.Products = _mapper.MapEnumerable<VendingMachineProductLink, ProductSetModel>(machine.VendingMachineProductLinks);
            return model;
        }
    }
}
